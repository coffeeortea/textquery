/*
 * Thread.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: haha
 */

#include "Thread.h"
#include <iostream>
#include <stdexcept>

using namespace std;

Thread::Thread() :
		_arg(NULL), _started(false), _id(-1)
{
}
Thread::~Thread()
{

}

void Thread::start(void *arg)
{
	int ret;
	if (_started == false)
	{
		_started = true;
		this->_arg = arg;
		ret = pthread_create(&_id, NULL, &Thread::exec, this);
		if (ret != 0)
			throw std::runtime_error("创建线程失败！");
		std::cout << "启动线程" << _id << std::endl;
	}
}

void Thread::join()
{
	pthread_join(_id, NULL);
}

void Thread::terminal()
{
	cout << "线程终止" << _id << endl;
	pthread_exit(NULL);
}

std::size_t Thread::get_tid() const
{
	return _id;
}

void *Thread::exec(void *arg)
{
	reinterpret_cast<Thread*>(arg)->run();
	return NULL;
}

