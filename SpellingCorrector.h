/*
 * SpellingCorrector.h
 *
 *  Created on: Mar 28, 2014
 *      Author: haha
 */

#ifndef SPELLINGCORRECTOR_H_
#define SPELLINGCORRECTOR_H_

#include <vector>
#include <set>
#include <string>
#include <map>
#include "StringUtils.h"
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <utility>
#include <iostream>
#include <algorithm>
#include "CacheTimerThread.h"

class SpellingCorrector
{
public:
	SpellingCorrector(const std::string &words, const std::string &stop)
	{
		read_file(words, stop);
		adjust();
		count_word();
		std::cout << _words.size() << std::endl;
		std::cout << _stop_set.size() << std::endl;
		std::cout << _word_count.size() << std::endl;
	}

	std::string run_query(const std::string &);
private:

	std::vector<std::string> _words;    //语料库
	std::map<std::string, std::size_t> _word_count;  //统计单词个数
	std::set<char> _stop_set;	//停用符号



	std::vector<std::pair<std::string, std::size_t> > _edit_one;  //编辑距离为1
	std::vector<std::pair<std::string, std::size_t> > _edit_two;  //编辑距离为2


	void read_file(const std::string &words, const std::string &stop);   // words && stop_list
	void adjust();
	void count_word();  // frequency of word

};

#endif /* SPELLINGCORRECTOR_H_ */
