/*
 * ThreadPool.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#include "ThreadPool.h"
#include <stdexcept>

ThreadPool::ThreadPool(std::size_t max_thread) :
		_is_started(false), _max_thread(max_thread), _thread_vector(
				_max_thread), _cond(&_lock)
{
	std::vector<WorkerThread>::iterator iter = _thread_vector.begin();
	while (iter != _thread_vector.end())
	{
		iter->register_thread_pool(this);
		++iter;
	}
}

ThreadPool::~ThreadPool()
{
	close();
}

void ThreadPool::start()
{
	if (_is_started == false)
	{
		_is_started = true;  //这句必须加到开头
		//启动所有的线程
		std::vector<WorkerThread>::iterator iter = _thread_vector.begin();
		while (iter != _thread_vector.end())
		{
			iter->start();
			++iter;
		}

	}
}

/*
 * 线程获取任务,注意查看线程池是否关闭
 */
bool ThreadPool::get_task(Task &task)
{
	_lock.lock();

	//必须用while
	while (_is_started == true && _task_queue.empty())
	{
		_cond.wait();
	}

	/*
	 * 这段有三种情况，
	 * 1.一开始线程池就是false
	 * 2.等待队列任务后线程池变成了false
	 * 3.线程池关闭时，线程被激活，此时为false
	 */
	if (_is_started == false)
	{
		_lock.unlock();
		return false;
	}
	task = _task_queue.front();
	_task_queue.pop();

	_lock.unlock();

	return true;

}

/*
 * 添加任务到线程池
 */
void ThreadPool::add_task(Task task)
{
	_lock.lock();
	_task_queue.push(task);
	_cond.notify();
	_lock.unlock();
}

void ThreadPool::close()
{
	if (_is_started == false)
//		throw std::runtime_error("线程池已经关闭！");
		return;//直接退出

	//关闭线程池
	_is_started = false;   //这句不用lock
	_lock.lock();
	_cond.notify_all();
	_lock.unlock();

	//等待所有线程关闭
	std::vector<WorkerThread>::iterator iter = _thread_vector.begin();
	while (iter != _thread_vector.end())
	{
		iter->join();
		++iter;
	}
	//清空队列
	while (!_task_queue.empty())
	{
		_task_queue.pop();
	}
}

bool ThreadPool::is_empty()
{
	_lock.lock();
	bool ret = _task_queue.empty();
	_lock.unlock();
	return ret;
}

std::size_t ThreadPool::get_size()
{
	_lock.lock();
	std::size_t ret = _task_queue.size();
	_lock.unlock();
	return ret;
}
