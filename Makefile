main: main.o stringutils.o spellingcorrector.o thread.o threadpool.o lock.o condition.o workerthread.o udpserver.o cachequery.o cachetimerthread.o
	g++ -o main.exe main.o stringutils.o spellingcorrector.o thread.o threadpool.o lock.o condition.o workerthread.o udpserver.o cachequery.o cachetimerthread.o -lpthread

main.o: main.cpp
	g++ -o main.o -c main.cpp

test: test.o cachequery.o lock.o thread.o cachetimerthread.o
	g++ -o test.exe test.o cachequery.o lock.o thread.o cachetimerthread.o -lpthread
test.o: test_main.cpp
	g++ -o test.o -c -g test_main.cpp 
stringutils.o: StringUtils.cpp
	g++ -o stringutils.o -c -g  StringUtils.cpp
spellingcorrector.o: SpellingCorrector.cpp
	g++ -o spellingcorrector.o -c -g SpellingCorrector.cpp
thread.o: Thread.cpp
	g++ -o thread.o -g -c Thread.cpp
threadpool.o: ThreadPool.cpp
	g++ -o threadpool.o -g -c ThreadPool.cpp
lock.o: Lock.cpp
	g++ -o lock.o -g -c Lock.cpp
condition.o: Condition.cpp
	g++ -o condition.o -g -c Condition.cpp
workerthread.o: WorkerThread.cpp
	g++ -o workerthread.o -g -c WorkerThread.cpp
udpserver.o: UDPServer.cpp
	g++ -o udpserver.o -c -g UDPServer.cpp
cachequery.o: CacheQuery.cpp
	g++ -o cachequery.o -c -g CacheQuery.cpp
cachetimerthread.o: CacheTimerThread.cpp
	g++ -o cachetimerthread.o -c -g CacheTimerThread.cpp
clean:
	rm -rf *.o *.exe core*
