/*
 * ThreadPool.h
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <list>
#include <queue>
#include <vector>
#include "Thread.h"
#include <iostream>
#include "Lock.h"
#include "Condition.h"
#include "WorkerThread.h"

class ThreadPool
{
public:
	ThreadPool(std::size_t max_thread);
	virtual ~ThreadPool();

	void start();			//启动线程池
	bool get_task(Task&);		//从任务队列中获取任务
	void add_task(Task);		//增加任务
	void close();			//关闭线程池

	bool is_empty();		//判断线程池是否为空
	std::size_t get_size(); //获取线程池任务的个数

private:
	std::size_t _max_thread;		//最大线程数量
	std::queue<Task> _task_queue;	//任务队列
	bool _is_started;				//线程是否开启
	std::vector<WorkerThread> _thread_vector;	//线程容器
	Lock _lock;						//互斥锁
	Condition _cond;				//等待任务的条件变量

	//禁止复制
	ThreadPool(const ThreadPool&);
	void operator=(const ThreadPool&);
};

#endif /* THREADPOOL_H_ */
