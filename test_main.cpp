/*
 * test_main.cpp
 *
 *  Created on: Mar 29, 2014
 *      Author: haha
 */

#include <iostream>
#include <string>
#include <set>
#include "CacheQuery.h"
#include "CacheTimerThread.h"

void sigHandler(int signal_no)
{

}

using namespace std;

int main()
{
	CacheQuery *p_cache = CacheQuery::get_instance(string("/tmp/textcache"));
	p_cache->add_word_to_cache(string("hello"), string("world"));
	CacheTimerThread pt(5);
	pt.start(NULL);

	sleep(5);
	p_cache->add_word_to_cache(string("hello1"), string("world"));
	sleep(5);
	p_cache->add_word_to_cache(string("hello2"), string("world"));
	sleep(5);
	p_cache->add_word_to_cache(string("hello3"), string("world"));
	sleep(5);
	p_cache->add_word_to_cache(string("hello4"), string("world"));
	sleep(5);
	p_cache->add_word_to_cache(string("hello5"), string("world"));
	sleep(5);
	p_cache->add_word_to_cache(string("hello6"), string("world"));

	while (1)
		;
	return 0;
}
