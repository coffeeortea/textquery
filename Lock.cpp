/*
 * Lock.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#include "Lock.h"
#include <stdexcept>

Lock::Lock()
{
	if(pthread_mutex_init(&_mutex, NULL) == -1)
		throw std::runtime_error("初始化锁失败！");
}

Lock::~Lock()
{
	pthread_mutex_destroy(&_mutex);
}

void Lock::lock()
{
	pthread_mutex_lock(&_mutex);
}

void Lock::unlock()
{
	pthread_mutex_unlock(&_mutex);
}
