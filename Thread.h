/*
 * Thread.h
 *
 *  Created on: Apr 1, 2014
 *      Author: haha
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>
#include <cstddef>

class Thread
{
public:
	Thread();
	virtual ~Thread();
	std::size_t get_tid() const;
	void start(void *arg = NULL);
	void join();
	void terminal();
	virtual void run() = 0;
	std::size_t tid() const;
private:
	pthread_t _id;

//	//禁止复制线程
//	Thread(const Thread&);
//	Thread& operator=(const Thread&);

protected:
	bool _started;   //线程是否启动
	void *_arg;		//线程参数
	static void *exec(void *thr);    //static函数，做函数指针用
};

#endif /* THREAD_H_ */
