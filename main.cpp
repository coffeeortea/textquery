#include <iostream>
#include <cstdlib>
#include <string>
#include "SpellingCorrector.h"
#include "UDPServer.h"
#include "ThreadPool.h"
#include "CacheQuery.h"
#include "CacheTimerThread.h"

using namespace std;

int main(int argc, char const **argv)
{

	clock_t start, finish;
	double totaltime;
	start = clock();

	string words = "/home/haha/big.txt";
	string stop = "stoplist.txt";
	SpellingCorrector sp(words, stop);

	finish = clock();
	totaltime = (double) (finish - start) / CLOCKS_PER_SEC;
	cout << "\n读取文本的运行时间为" << totaltime << "秒！" << endl;

	//生成cache对象
	CacheQuery *p_cache = CacheQuery::get_instance(string("/tmp/textcache"));
	//设置扫描线程
	CacheTimerThread pt(5);    //5秒钟扫描一次
	pt.start(NULL);

	ThreadPool pool(3);
	UDPServer server(8888, &pool, &sp);

	pool.start();
	server.start();

	while (true)
		;
	pool.close();

	return 0;

}
