/*
 * StringUtils.cpp
 *
 *  Created on: Mar 28, 2014
 *      Author: haha
 */

#include "StringUtils.h"

//此函数仅限本文件使用
static std::size_t _min(std::size_t a, std::size_t b, std::size_t c)
{
	std::size_t _tmp = a;
	_tmp = (_tmp < b) ? _tmp : b;
	_tmp = (_tmp < c) ? _tmp : c;
	return _tmp;
}

std::string &StringUtils::remove_stop_char(std::string &s,
		const std::set<char> &stop_set)
{
	std::string::iterator it = s.begin();
	while (it != s.end())
	{
		if (stop_set.count(*it))
		{
			it = s.erase(it);
		}
		else
		{
			++it;
		}
	}

	return s;
}
std::string &StringUtils::string_to_lower(std::string &s)
{
	std::string::iterator it = s.begin();
	while (it != s.end())
	{
		if (std::isupper(*it))
		{
			*it = std::tolower(*it);
		}
		++it;
	}
	return s;
}
std::size_t StringUtils::edit_distance(const std::string &s1,
		const std::string &s2)
{
	using namespace std;
	std::vector<std::vector<std::size_t> > memo(s1.size() + 1,
			std::vector<std::size_t>(s2.size() + 1, 0)); //二维数组
	memo[0][0] = 0;
	for (std::size_t ix = 0; ix <= s1.size(); ++ix)
	{
		memo[ix][0] = ix;
	}
	for (std::size_t iy = 0; iy <= s2.size(); ++iy)
	{
		memo[0][iy] = iy;
	}
	for (std::size_t ix = 1; ix <= s1.size(); ++ix)
	{
		for (std::size_t iy = 1; iy <= s2.size(); ++iy)
		{

			if (s1[ix-1] == s2[iy-1])
			{
				memo[ix][iy] = memo[ix - 1][iy - 1];
			}
			else
			{
				memo[ix][iy] = _min(memo[ix - 1][iy] + 1,
						memo[ix - 1][iy - 1] + 1, memo[ix][iy - 1] + 1);
			}
		}
	}
	return memo[s1.size()][s2.size()];
}
