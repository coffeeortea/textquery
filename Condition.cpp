/*
 * Condition.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#include "Condition.h"
#include <stdexcept>

Condition::Condition(Lock* p_lock) :
		_p_lock(p_lock)
{
	if (pthread_cond_init(&_cond, NULL) == -1)
		throw std::runtime_error("初始化条件变量失败");
}

Condition::~Condition()
{
	pthread_cond_destroy(&_cond);
}

void Condition::wait()
{
	pthread_cond_wait(&_cond, &_p_lock->_mutex);
}
void Condition::notify_all()
{
	pthread_cond_broadcast(&_cond);
}

void Condition::notify()
{
	pthread_cond_signal(&_cond);
}
