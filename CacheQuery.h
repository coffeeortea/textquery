/*
 * CacheQuery.h
 *
 *  Created on: Apr 3, 2014
 *      Author: haha
 */

#ifndef CACHEQUERY_H_
#define CACHEQUERY_H_

#include <hash_map>
#include <string>
#include <utility>
#include "Lock.h"
#include <cstring>
#include <functional>

/*
 * 应该用单例模式实现
 *
 */

namespace __gnu_cxx
{
template<> struct hash<std::string>
{
	std::size_t operator()(const std::string &s) const
	{
		hash<const char *> h;
		return h(s.c_str());
	}
};
}

class CacheQuery
{
public:
	typedef std::string __key;
	typedef std::string __value;
	typedef std::string __filename;

	virtual ~CacheQuery();				//析构的时候记得写入磁盘

	void read_cache_file();   //从文件中读取缓存
	void add_word_to_cache(const __key &, const __value &); //增加键值对到缓存
	void add_word_to_cache(std::pair<__key, __value >);  //重载插入操作

	__value query_word_in_cache(const __key &) const;   //查询单词
	bool replace_word_in_cache(const __key &, const __value &); //替换缓存
	bool delete_word_in_cache(const __key &);
	void save_cache_to_file();			//将缓存写入磁盘
	bool is_modified() const;					//cache是否被修改过

	//用于获取Singleton的实例
	static CacheQuery *get_instance(const __filename &);

private:
	//构造函数设为私有，防止从外面生成对象
	CacheQuery(const __filename &);
	__gnu_cxx ::hash_map<__key, __value, __gnu_cxx ::hash<std::string> > _cache_words;//实际缓存
	__filename _filename;		//磁盘文件名字
	bool _is_modified;		//是否被修改过

	static Lock _lock;		//用于单例模式
	static CacheQuery *_p_instance;

	//重新命名迭代器
	typedef __gnu_cxx ::hash_map<__key, __value >::iterator __hash_map_iter;
	typedef __gnu_cxx ::hash_map<__key, __value >::const_iterator __hash_map_iter_const;

	//禁止复制
	CacheQuery(const CacheQuery&);
	void operator=(const CacheQuery&);
};

#endif /* CACHEQUERY_H_ */
