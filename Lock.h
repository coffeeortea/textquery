/*
 * Lock.h
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#ifndef LOCK_H_
#define LOCK_H_

#include <pthread.h>

class Condition;

class Lock
{
public:
	Lock();
	virtual ~Lock();

	void lock();
	void unlock();

	friend class Condition;	  //Condition是友元类


private:
	pthread_mutex_t _mutex;

	Lock(const Lock &);
	Lock& operator=(const Lock&);

};

#endif /* LOCK_H_ */
