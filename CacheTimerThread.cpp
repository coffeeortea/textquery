/*
 * CacheTimerThread.cpp
 *
 *  Created on: Apr 3, 2014
 *      Author: haha
 */

#include "CacheTimerThread.h"

CacheTimerThread::CacheTimerThread(std::size_t interval) :
		_interval(interval)
{

}

CacheTimerThread::~CacheTimerThread()
{
}

void CacheTimerThread::run()
{
	while (true)
	{
		sleep(_interval);
		process();
	}
}

void CacheTimerThread::process()
{
	//此时cache肯定生成了对象
	CacheQuery *p_cache_query = CacheQuery::get_instance(std::string(""));
	p_cache_query->save_cache_to_file();
}

