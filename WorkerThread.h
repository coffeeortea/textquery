/*
 * WorkerThread.h
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#ifndef WORKERTHREAD_H_
#define WORKERTHREAD_H_

#include "Thread.h"
#include <string>
#include <netinet/in.h>
#include "Lock.h"
#include "SpellingCorrector.h"

class ThreadPool;

class Task
{
public:
	int _sockfd;
	SpellingCorrector *_spelling_corrector;
	std::string _query_word;		//查询词
	struct sockaddr_in _des_addr;	//查询结果返回的地址
};

class WorkerThread: public Thread
{
public:
	WorkerThread();
	virtual ~WorkerThread();
	virtual void run();

	void register_thread_pool(ThreadPool *pool); //注册线程池
	void work(Task&);

private:
	ThreadPool *_pool;

//	//禁止复制
//	WorkerThread(const WorkerThread&);
//	void operator=(const WorkerThread&);

};

#endif /* WORKERTHREAD_H_ */
