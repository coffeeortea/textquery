/*
 * WorkerThread.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#include "WorkerThread.h"
#include "ThreadPool.h"
#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>

//Lock WorkerThread::_lock;

WorkerThread::WorkerThread() :
		_pool(NULL)
{
}

WorkerThread::~WorkerThread()
{
}

void WorkerThread::run()
{
	if (_pool == NULL)
		throw std::runtime_error("线程池还没有绑定！");
	Task task;
	bool ret;
	while (true)
	{
		ret = _pool->get_task(task);
		if (ret == false)
		{
			terminal();  //终止线程
		}
		else
		{
			work(task);	 //执行查询
		}
	}
}
void WorkerThread::work(Task &task)
{
	std::cout << "查询服务: " << task._query_word << std::endl;
	std::string ret = task._spelling_corrector->run_query(task._query_word);
	char *buf = new char[ret.size() + 1];
	std::strcpy(buf, ret.c_str());
	int n_send = sendto(task._sockfd, buf, std::strlen(buf), 0,
			(struct sockaddr*) &task._des_addr, sizeof(task._des_addr));
	if (n_send < 0)
		throw std::runtime_error("发送结果失败！");
	delete[] buf;  //释放内存
	buf = NULL;
}

void WorkerThread::register_thread_pool(ThreadPool *pool)
{
	_pool = pool;
}

