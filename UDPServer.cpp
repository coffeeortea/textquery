/*
 * UDPServer.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: haha
 */

#include "UDPServer.h"

UDPServer::UDPServer(int port, ThreadPool* pool, SpellingCorrector* spelling_corrector)
{
	_fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (_fd < 0)
		throw std::runtime_error("create socket failed!");
	_port = port;
	_server_addr.sin_port = _port;
	_server_addr.sin_family = AF_INET;
	_server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	_bind();		//绑定端口
	register_thread_pool(pool);
	register_spell_corrector(spelling_corrector);
}

void UDPServer::_bind()
{
	if (bind(_fd, (struct sockaddr*) &_server_addr, sizeof(struct sockaddr_in))
			< 0)
		throw std::runtime_error("bind failed!");
}

void UDPServer::start()
{
	while (1)
	{
		process();
	}
}
void UDPServer::process()
{
	struct sockaddr_in client_addr;
	std::size_t clientlen = sizeof(client_addr);
	char buf[1024];
	int nRead;

	if ((nRead = recvfrom(_fd, buf, 1023, 0, (struct sockaddr*) &client_addr,
			&clientlen)) > 0)
	{
		buf[nRead] = 0;
		Task tmp_task;
		tmp_task._sockfd = _fd;
		tmp_task._query_word = buf;
		tmp_task._des_addr = client_addr;
		tmp_task._spelling_corrector = _spelling_corrector;

		_pool->add_task(tmp_task);  //把任务加入线程池

		//process

	}
}

//注册线程池
void UDPServer::register_thread_pool(ThreadPool *pool)
{
	_pool = pool;
}
void UDPServer::register_spell_corrector(SpellingCorrector* spellingcorrector)
{
	_spelling_corrector = spellingcorrector;
}

UDPServer::~UDPServer()
{
	close(_fd);
}
