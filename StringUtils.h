/*
 * StringUtils.h
 *
 *  Created on: Mar 28, 2014
 *      Author: haha
 */

#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <string>
#include <set>
#include <cstdlib>
#include <vector>

class StringUtils
{
private:

public:
	//移除标点符号
	static std::string &remove_stop_char(std::string &, const std::set<char> &);
	//转化为小写
	static std::string &string_to_lower(std::string &);
	//计算编辑距离
	static std::size_t edit_distance(const std::string &, const std::string &);
};

#endif /* STRINGUTILS_H_ */
