/*
 * Condition.h
 *
 *  Created on: Apr 2, 2014
 *      Author: haha
 */

#ifndef CONDITION_H_
#define CONDITION_H_

#include "Lock.h"

class Condition
{
public:
	Condition(Lock* p_lock);
	virtual ~Condition();
	void wait();
	void notify_all();
	void notify();

protected:
	pthread_cond_t _cond;
	Lock *_p_lock;
private:
	//禁止复制
	Condition(const Condition&);
	void operator=(const Condition&);
};

#endif /* CONDITION_H_ */
