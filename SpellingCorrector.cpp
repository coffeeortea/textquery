/*
 * SpellingCorrector.cpp
 *
 *  Created on: Mar 28, 2014
 *      Author: haha
 */

#include "SpellingCorrector.h"

static std::ifstream &open_file(std::ifstream &in, const std::string &file_name)
{
	in.close();
	in.clear();
	in.open(file_name.c_str());
	return in;
}

static bool cmp_pair(std::pair<std::string, std::size_t> left,
		std::pair<std::string, std::size_t> right)
{
	return left.second > right.second;
}

//读取文件
void SpellingCorrector::read_file(const std::string &words,
		const std::string &stop)
{
	std::ifstream infile;
	if (!open_file(infile, words))
	{
		throw std::runtime_error("can not open words");
	}
	_words.clear();
	_stop_set.clear();
	std::string line;
	std::string word;

	//int global = 0;

	while (std::getline(infile, line))
	{
		std::istringstream stream(line);
		while (stream >> word)
		{
			//std::cout << ++global << ": " << word << std::endl;
			_words.push_back(word);
		}
	}
	if (!open_file(infile, stop))
	{
		throw std::runtime_error("can not open stop");
	}

	//global = 0;

	while (std::getline(infile, line))
	{
		std::istringstream stream(line);
		while (stream >> word)
		{
			//std::cout << ++global << ": " << word << std::endl;
			_stop_set.insert(word[0]);
		}
	}
	infile.close();
	infile.clear();
}

//调整单词
void SpellingCorrector::adjust()
{
	std::vector<std::string>::iterator iter = _words.begin();
	while (iter != _words.end())
	{
		*iter = StringUtils::remove_stop_char(*iter, _stop_set);
		*iter = StringUtils::string_to_lower(*iter);

		//std::cout << *iter << std::endl;

		if (*iter == "")
		{
			iter = _words.erase(iter);
		}
		else
		{
			++iter;
		}
	}
}

//统计单词个数
void SpellingCorrector::count_word()
{
	_word_count.clear();
	std::vector<std::string>::const_iterator iter = _words.begin();
	while (iter != _words.end())
	{
		std::pair<std::map<std::string, std::size_t>::iterator, bool> ret =
				_word_count.insert(std::make_pair(*iter, 1));
		if (!ret.second)
		{
			++ret.first->second;
		}
		//std::cout << *iter << ": " <<  ret.first->second << std::endl;

		++iter;
	}
}
/*
 * 先去cache里面查找，
 * 如果里面找不到，再去自己查找
 */
std::string SpellingCorrector::run_query(const std::string &query_word)
{
	//先去查看缓存
	CacheQuery *p_cache = CacheQuery::get_instance(std::string(""));
	std::string cache_result = p_cache->query_word_in_cache(query_word);
	if (cache_result != "")
	{
		return cache_result;   //cache中存在结果
	}

	//清空上次的查询结果
	_edit_one.clear();
	_edit_two.clear();

	std::string result;

	std::map<std::string, std::size_t>::const_iterator iter =
			_word_count.begin();
	while (iter != _word_count.end())
	{

		if (query_word == iter->first)
		{
			result = query_word;
			break;
		}
		std::size_t distance = StringUtils::edit_distance(query_word,
				iter->first);

		if (distance == 1)
		{
			_edit_one.push_back(std::make_pair(iter->first, iter->second));
		}
		if (distance == 2)
		{
			_edit_two.push_back(std::make_pair(iter->first, iter->second));
		}

		++iter;
	}

	std::sort(_edit_one.begin(), _edit_one.end(), cmp_pair);
	std::sort(_edit_two.begin(), _edit_two.end(), cmp_pair);

	if (!_edit_one.empty())
	{
		result = _edit_one.front().first;
	}
	else if (!_edit_two.empty())
	{
		result = _edit_two.front().first;
	}
	else
	{
		result = "";
	}

	return result;
}
