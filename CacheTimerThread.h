/*
 * CacheTimerThread.h
 *
 *  Created on: Apr 3, 2014
 *      Author: haha
 */

#ifndef CACHETIMERTHREAD_H_
#define CACHETIMERTHREAD_H_

#include "Thread.h"
#include "Lock.h"
#include "CacheQuery.h"
#include <unistd.h>
#include <iostream>
#include <stdexcept>

/*
 * 这个类采用单例模式实现
 *	Singleton
 */
class CacheTimerThread: public Thread
{
public:
	CacheTimerThread(std::size_t interval);
	virtual ~CacheTimerThread();
	virtual void run();


private:
	std::size_t _interval;   //时间间隔
	void process();  //具体处理事务

	//禁止复制
	CacheTimerThread(const CacheTimerThread&);
	void operator=(const CacheTimerThread&);
};

#endif /* CACHETIMERTHREAD_H_ */
