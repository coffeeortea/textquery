/*
 * CacheQuery.cpp
 *
 *  Created on: Apr 3, 2014
 *      Author: haha
 */

#include "CacheQuery.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>

//static
static std::ifstream &open_file(std::ifstream &in, const std::string &file_name)
{
	in.close();
	in.clear();
	in.open(file_name.c_str());
	return in;
}

static std::ofstream &open_file(std::ofstream &out,
		const std::string &file_name)
{
	out.close();
	out.clear();
	out.open(file_name.c_str());
	return out;
}

CacheQuery::CacheQuery(const __filename &filename) :
		_filename(filename), _is_modified(false)
{
	read_cache_file();
}

CacheQuery::~CacheQuery()
{
	save_cache_to_file();		//析构之前记得保存
}

void CacheQuery::read_cache_file()
{
	std::ifstream infile;
	if (!open_file(infile, _filename))
	{
		throw std::runtime_error("can not open cache file!");
	}
	__key key;
	__value value;

	std::string line;
	while (std::getline(infile, line))
	{
		std::istringstream stream(line);
		while (stream >> key >> value)
		{
			add_word_to_cache(key, value);
		}
	}
	infile.close();
	infile.clear();
}

void CacheQuery::add_word_to_cache(const __key &key, const __value &value)
{
	if (key == "" | value == "")
		return;
	std::pair<__hash_map_iter, bool> ret = _cache_words.insert(
			std::make_pair(key, value));
	if (ret.second == false)		//插入失败
	{
		replace_word_in_cache(key, value);		//执行替换算法
	}
	else
	{
		_is_modified = true;   //插入成功的情况
	}
}

void CacheQuery::add_word_to_cache(std::pair<__key, __value> key_value)
{
	add_word_to_cache(key_value.first, key_value.second);
}

CacheQuery::__value CacheQuery::query_word_in_cache(const __key &key) const
{
	__hash_map_iter_const iter = _cache_words.find(key);
	if (iter != _cache_words.end())
	{
		return iter->second;
	}
	else
	{
		return __value("");
	}
}

bool CacheQuery::replace_word_in_cache(const __key &key, const __value &value)
{

	if (key == "" | value == "")
		return false;
	_cache_words[key] = value;
	_is_modified = true;
	return true;
}

bool CacheQuery::delete_word_in_cache(const __key &key)
{
	__hash_map_iter iter = _cache_words.find(key);
	if (iter != _cache_words.end())
	{
		_cache_words.erase(iter);
		_is_modified = true;
		return true;
	}
	else
	{
		return false;
	}
}

//将文件保存到磁盘
void CacheQuery::save_cache_to_file()
{
	if (_is_modified == false)
	{
		return;   	//没有修改过就直接退出，不保存磁盘
	}

	std::ofstream outfile;
	if (!open_file(outfile, _filename))
	{
		throw std::runtime_error("can not open cache file!");
	}
	__hash_map_iter_const iter = _cache_words.begin();
	while (iter != _cache_words.end())
	{
		outfile << iter->first << " " << iter->second << std::endl;
		++iter;
	}
	outfile.close();
	outfile.clear();
	_is_modified = false;
	std::cout << "写入磁盘" << std::endl;
}

bool CacheQuery::is_modified() const
{
	return _is_modified;
}
Lock CacheQuery::_lock;  //初始化静态锁
CacheQuery *CacheQuery::_p_instance = NULL;

CacheQuery *CacheQuery::get_instance(const __filename &filename)
{
	if (_p_instance == NULL)
	{
		_lock.lock();
		if (_p_instance == NULL)
		{
			_p_instance = new CacheQuery(filename);
		}
		_lock.unlock();
	}
	return _p_instance;
}
