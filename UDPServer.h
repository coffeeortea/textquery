/*
 * UDPServer.h
 *
 *  Created on: Apr 1, 2014
 *      Author: haha
 */

#ifndef UDPSERVER_H_
#define UDPSERVER_H_

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdexcept>
#include <string>
#include "ThreadPool.h"
#include "SpellingCorrector.h"

class UDPServer
{
public:
	UDPServer(int port, ThreadPool*, SpellingCorrector*);
	~UDPServer();

	void start();

private:
	int _fd;
	struct sockaddr_in _server_addr;
	int _port;

	SpellingCorrector *_spelling_corrector;
	ThreadPool *_pool;    //线程池
	void register_thread_pool(ThreadPool *); //注册线程池
	void register_spell_corrector(SpellingCorrector*);
	void _bind();			//绑定端口
	void process();			//具体工作函数
};

#endif /* UDPSERVER_H_ */
